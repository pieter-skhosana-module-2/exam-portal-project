***********EXAM PORTAL***********
This project was developed as the final year software development system for the Diploma: IT (2021)
Allows for written examinations 

******************************FUNCTIONALITY******************************
********Student UI*********
- login (authenticates from the users table in the database)
- download question paper (saved in a directory on the server)
    - enter module code to generate link
- Timer counts the remaining duration until exam finishes
- upload exam script (registers in the database and saves the doc in a directory on the server)
    - enter module code and browse for the script to upload
    - student must declare own work before submission
- display submisison message if success or not

********ADMIN UI**********
- login 
- Dashboard with links and enrollments dynamic total counts
    - link 1: upload question paper
    - link 2: exam submissions 
    - link 3: uploads per module
    - link 4: exam schedule
    - link 5: exam timetable predictions (allows to set for flexible time and date slots) 

*******DATABASE (MySQL - PHPMyAdmin)********
consists of 7 tables
1. admin – this table is for the admin staff from the exam department and their login credentials. They will be scheduling exams and have access to the MIS reports.
2. enrollments – contains all the students enrolled for exams per module.
3. exams – contains all the exams submitted by the students.
4. exam_papers – this table is for the exam question papers as uploaded by the admin staff.
5. modules – table for all the modules that will be scheduled for exams.
6. students – contains students information.
7. users – contains the login credentials for students.
